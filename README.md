# K8s-react

## Backend
```
docker run -dt -p 3000:3000 --name=k8snode \
-e NAME=api \
-e MONGO_URI=mongodb://<user>:<password>@<host>:<port> \
-e API_URL=<api_url> \
k8snode
```

## Frontend
```
REACT_APP_API_URL=http://localhost:3000 yarn start
REACT_APP_API_URL=http://localhost:3000 yarn test
```