import React, { useState, useEffect } from 'react';
import './App.css';
import { Container, Grid } from '@material-ui/core';
import Section from './components/Section';
import Info from './components/Info';
import CallApi from './components/CallApi';
import DbConnect from './components/DbConnect';
import DbDisconnect from './components/DbDisconnect';
import { getInfo, callApi, dbDisconnect, dbConnect, delay } from './api'


function App() {
  const [info, setInfo] = useState({
    loading: false,
    data: null,
  })

  const [call, setCall] = useState({
    loading: false,
    data: null,
  })

  const [callUrl, setCallUrl] = useState('')
  const [connect, setConnect] = useState({
    loading: false,
    data: null,
  })

  const [connectUri, setConnectUri] = useState('')
  const [disconnect, setDisconnect] = useState({
    loading: false,
    data: null,
  })

  const handleCallApi = async () => {
    setInfo({
      ...info,
      loading: true,
      // data: null,
    })
    const data = await getInfo();
    await delay(1000);
    setInfo({
      loading: false,
      data,
    })
  }

  useEffect(handleCallApi, [])

  return (
    <Container maxWidth='md'>
      <Grid container spacing={3}>
        <Section>
          <Info
            data={info.data}
            isLoading={info.loading}
            onClick={handleCallApi}
          />
        </Section>

        <Section>
          <DbDisconnect
            data={disconnect.data}
            isLoading={disconnect.loading}
            onClick={async () => {
              setDisconnect({
                loading: true,
                data: null,
              })
              const data = await dbDisconnect();
              await delay(1000);
              setDisconnect({
                loading: false,
                data,
              })
              handleCallApi()
            }}
          />
        </Section>
        <Section>
          <DbConnect
            data={connect.data}
            isLoading={connect.loading}
            uri={connectUri}
            onClick={async () => {
              setConnect({
                loading: true,
                data: null,
              })
              const data = await dbConnect({ uri: connectUri });
              await delay(1000);
              setConnect({
                loading: false,
                data,
              })
              handleCallApi()
            }}
            onInputChange={e => setConnectUri(e.target.value)}
          />
        </Section>
        <Section>
          <CallApi
            data={call.data}
            isLoading={call.loading}
            url={callUrl}
            onClick={async () => {
              setCall({
                loading: true,
                data: null,
              })
              const data = await callApi({ url: callUrl });
              await delay(1000);
              setCall({
                loading: false,
                data,
              })
            }}
            onInputChange={e => setCallUrl(e.target.value)}
          />
        </Section>
      </Grid>
    </Container>
  );
}

export default App;
