import React from 'react'
import { Box, TextField } from '@material-ui/core'
import Code from './Code'
import TButton from './TButton'

export default function ({
	data,
	isLoading,
	url,
	onClick,
	onInputChange,
}) {
	return (
		<Box p={3}>
			<Box marginBottom={1}>
				<TextField label='Url' fullWidth value={url} placeholder='https://xxx.xxx/xxx' onChange={onInputChange} />
			</Box>
			<TButton isLoading={isLoading} title='Call Api' onClick={onClick} />
			<Code code={data} />
		</Box>
	)
}
