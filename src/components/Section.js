import React from 'react';
import { Grid, Paper } from '@material-ui/core';

export default function({ children }) {
	return (
		<Grid item xs={12} >
			<Paper elevation={6}>{children}</Paper>
		</Grid>
	)
}
