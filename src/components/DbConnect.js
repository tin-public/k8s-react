import React from 'react'
import { Box, TextField } from '@material-ui/core'
import Code from './Code'
import TButton from './TButton'

export default function ({
	data,
	isLoading,
	uri,
	onClick,
	onInputChange,
}) {
	return (
		<Box p={3}>
			<Box marginBottom={1}>
				<TextField label='Uri' fullWidth value={uri} placeholder='mongodb://<user>:<password>@<host>:<port>' onChange={onInputChange} />
			</Box>
			<TButton isLoading={isLoading} title='Db Connect' onClick={onClick} />
			<Code code={data} />
		</Box>
	)
}
