import React from 'react'
import { Box } from '@material-ui/core'
import Code from './Code'
import TButton from './TButton'

export default function ({
	data,
	isLoading,
	onClick,
}) {
	return (
		<Box p={3}>
			<TButton isLoading={isLoading} title='Check' onClick={onClick} />
			<Code code={data} />
		</Box>
	)
}
