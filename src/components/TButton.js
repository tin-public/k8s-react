import React from 'react'
import { Box, Button, CircularProgress } from '@material-ui/core'

export default function ({
	isLoading,
	title,
	onClick,
}) {
	return (
		<Box height={35} clone>
			{
				isLoading ? <Button variant="contained" color="primary" disabled><CircularProgress size={20} /></Button>
					: <Button variant="contained" color="primary" onClick={onClick}>{title}</Button>
			}
		</Box>
	)
}