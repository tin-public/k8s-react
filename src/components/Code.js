import React from 'react'
import { Box, Typography } from '@material-ui/core';

export default function Code({
	code,
}) {
	return (
		<Box>
			<Box marginTop={1}>
				<Typography variant="h6">Result:</Typography>
			</Box>
			<Box clone margin={0}>
				<pre>
					<Box fontFamily='Monospace' bgcolor='#ddd' p={1} overflow='wrap'>
						{JSON.stringify(code, null, 2)}
					</Box>
				</pre>
			</Box>
		</Box>
	)
}
