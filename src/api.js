async function ft({
	path = '/',
	body,
	query,
	method,
	...rest
} = {}) {
	let headers = body ? {
		'Content-Type': 'application/json'
	} : {}
	if (rest.headers) {
		headers = {...headers, ...rest.headers}
	}

	const bodyString = body ? JSON.stringify(body) : null

	const queryString = query ? `?${new URLSearchParams(query).toString()}` : ''

	const response = await fetch(
		`${process.env.REACT_APP_API_URL}${path}${queryString}`,
		{
			headers,
			method,
			body: bodyString,
		}
	)

	return response.json()
}

export function getInfo() {
	return ft()
}

export function callApi(data) {
	return ft({
		path: '/api-call',
		method: 'post',
		body: data,
	})
}

export function dbConnect(data) {
	return ft({
		path: '/db-connect',
		method: 'post',
		body: data,
	})
}

export function dbDisconnect() {
	return ft({
		path: '/db-disconnect',
		method: 'post',
	})
}

export function delay(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}
