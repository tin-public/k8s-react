import { getInfo, callApi, dbDisconnect, dbConnect } from './api'
test('getinfo ok', async () => {
	const result = await getInfo();
	expect(result.isRunnung).toBeTruthy();
})

test('callApi ok', async () => {
	const result = await callApi({
		// url: 'xxx',
	});
	expect(result.result).toBeTruthy();
})

test('dbDisconnect ok', async () => {
	const result = await dbDisconnect();
	expect(result.disconnected).toBeTruthy();
})

test('dbConnect ok', async () => {
	const result = await dbConnect({
		// "uri": "xxx"
	});
	expect(result.connected).toBeTruthy();
})
