FROM node:12.16-alpine AS build
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn
COPY public ./public
COPY src ./src
ARG REACT_APP_API_URL
RUN yarn build

FROM nginx:1.17-alpine
COPY --from=build /app/build /usr/share/nginx/html
